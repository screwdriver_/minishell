# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: llenotre <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/01/23 17:18:39 by llenotre          #+#    #+#              #
#    Updated: 2019/02/04 20:20:25 by llenotre         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = minishell
CC = gcc
CFLAGS = -Wall -Wextra -Werror

SRC_DIR = src/
DIRS := $(shell find src/ -type d)
SRC =	src/command/cd.c\
		src/command/echo.c\
		src/command/env.c\
		src/command/setenv.c\
		src/command/unsetenv.c\
		src/controls.c\
		src/env_var.c\
		src/exec.c\
		src/file.c\
		src/main.c\
		src/parse.c\
		src/signal.c\
		src/split.c
HDR =	src/minishell.h

OBJ_DIR = obj/
OBJ_DIRS := $(patsubst $(SRC_DIR)%,$(OBJ_DIR)%,$(DIRS))
OBJ := $(patsubst $(SRC_DIR)%.c,$(OBJ_DIR)%.o,$(SRC))

LIBFT = libft/libft.a

all: $(NAME) tags

$(NAME): $(LIBFT) $(OBJ_DIRS) $(OBJ)
	$(CC) $(CFLAGS) -o $(NAME) $(OBJ) $(LIBFT)

$(LIBFT):
	make -C libft/

$(OBJ_DIRS):
	mkdir -p $(OBJ_DIRS)

$(OBJ_DIR)%.o: $(SRC_DIR)%.c $(HDR)
	$(CC) $(CFLAGS) -o $@ -c $<

tags: $(SRC) $(HDR)
	ctags $(SRC) libft/*

count:
	find $(SRC_DIR) -type f -name "*.[ch]" | xargs wc -l

clean:
	rm -rf $(OBJ_DIR)
	make clean -C libft/

fclean: clean
	rm -f $(NAME)
	rm -f tags
	make fclean -C libft/

re: fclean all

.PHONY: all clean fclean re
