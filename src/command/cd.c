/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/23 17:19:11 by llenotre          #+#    #+#             */
/*   Updated: 2019/02/04 19:14:37 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../minishell.h"

static const char	*get_home(char **environ)
{
	const char	*home;

	if (!(home = get_env_value(environ, "HOME")))
		ft_putendl_fd("cd: HOME not set", 2);
	return (home);
}

static const char	*get_path(char **args, char **environ)
{
	if (!args)
		return (NULL);
	if (!(*args))
		return (ft_strdup(get_home(environ)));
	else if (**args == '~')
	{
		return (ft_path_merge(get_home(environ), (*args) + 1));
	}
	else
		return (ft_strdup(*args));
}

void				c_cd(char **args, char ***environ)
{
	const char	current[PATH_MAX];
	const char	*path;
	int			err;

	if (!args || !environ)
		return ;
	if (!(path = get_path((char **)args + 1, *environ)))
		return ;
	if (!(err = check_file_error(path, FALSE, TRUE)))
	{
		ft_bzero((void *)current, PATH_MAX);
		if ((getcwd((void *)current, PATH_MAX)))
			set_env_value(environ, "OLDPWD", current);
		chdir(path);
		access(path, S_IRUSR);
		ft_bzero((void *)current, PATH_MAX);
		if ((getcwd((void *)current, PATH_MAX)))
			set_env_value(environ, "PWD", current);
	}
	else
	{
		ft_putstr_fd("cd: ", 2);
		print_file_error(path, err, FALSE);
	}
	free((void *)path);
}
