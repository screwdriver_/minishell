/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setenv.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/23 17:19:31 by llenotre          #+#    #+#             */
/*   Updated: 2019/02/04 18:11:58 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../minishell.h"

void	c_setenv(char **args, char ***environ)
{
	char *s;

	if (!args)
		return ;
	++args;
	if (ft_arrlen((void **)args) != 2
		&& !(ft_arrlen((void **)args) == 1 && ft_strchr(*args, '=')))
		return ;
	if ((s = ft_strchr(*args, '=')))
	{
		*(s++) = '\0';
		set_env_value(environ, *args, s);
	}
	else
		set_env_value(environ, args[0], args[1]);
}
