/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   echo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/23 17:19:24 by llenotre          #+#    #+#             */
/*   Updated: 2019/02/04 18:10:57 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../minishell.h"

static char	**parse_flag(char **args, t_bool *n)
{
	char *a;

	if (!args)
		return (NULL);
	*n = FALSE;
	while (*args || **args == '-')
	{
		if (!((*args)[1]))
			return (args + 1);
		a = (*args) + 1;
		while (*a)
		{
			if (*(a++) != 'n')
				return (args);
		}
		*n = TRUE;
		++args;
	}
	return (args);
}

void		c_echo(char **args, char ***environ)
{
	t_bool n;

	(void)environ;
	if (!args)
		return ;
	++args;
	if (!(args = (parse_flag(args, &n))))
		return ;
	while (*args)
	{
		ft_putstr(*(args++));
		if (*args)
			ft_putchar(' ');
	}
	if (!n)
		ft_putchar('\n');
}
