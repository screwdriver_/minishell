/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   unsetenv.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/23 17:19:31 by llenotre          #+#    #+#             */
/*   Updated: 2019/02/04 19:15:17 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../minishell.h"

static size_t	get_array_len(char **arr)
{
	size_t len;

	len = 0;
	while (arr[len])
		++len;
	return (len);
}

void			c_unsetenv(char **args, char ***environ)
{
	size_t	arr_len;
	char	**i;

	if (!args || !environ)
		return ;
	arr_len = get_array_len(*environ);
	while (*args)
	{
		if ((i = find_env_key(*environ, *args)))
			*environ = (char **)ft_arrremove((void **)*environ, i - *environ);
		++args;
		--arr_len;
	}
}
