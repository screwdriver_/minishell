/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/23 17:19:30 by llenotre          #+#    #+#             */
/*   Updated: 2019/02/04 19:31:09 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../minishell.h"

static char		**parse_flags(char **args, t_bool *ignore_env, t_bool *verbose)
{
	char *s;

	if (!args || !ignore_env || !verbose)
		return (NULL);
	*ignore_env = FALSE;
	*verbose = FALSE;
	while (*args)
	{
		if (ft_strequ(*args, "--"))
		{
			++args;
			break ;
		}
		if (**args != '-')
			break ;
		s = *(args++);
		while (*s)
		{
			if (*s == 'i')
				*ignore_env = TRUE;
			if (*(s++) == 'v')
				*verbose = TRUE;
		}
	}
	return (args);
}

static char		**parse_vars(char **args, char ***environ,
	const t_bool verbose)
{
	if (!environ)
		return (NULL);
	if (!(*environ) && !(*environ = ft_memalloc(sizeof(char *))))
		return (args);
	while (*args)
	{
		if (!ft_strchr(*args, '='))
			break ;
		if (verbose)
		{
			ft_putstr("#env setenv:\t");
			ft_putendl(*args);
		}
		*environ = (char **)ft_arrinsert((void **)*environ,
			(void *)ft_strdup(*args));
		++args;
	}
	return (args);
}

static void		env_exec(char **args, char ***environ, const t_bool verbose)
{
	char	**a;

	if (verbose)
	{
		ft_putstr("#env executing: ");
		ft_putendl(*args);
		a = args;
		while (*a)
		{
			ft_putstr("#env\targ[");
			ft_putnbr(a - args);
			ft_putstr("]= '");
			ft_putstr(*(a++));
			ft_putendl("'");
		}
	}
	exec(args, environ);
}

static void		list_env(char **environ)
{
	char	**a;

	a = environ;
	while (*a)
		ft_putendl(*(a++));
}

void			c_env(char **args, char ***environ)
{
	t_bool	ignore_env;
	t_bool	verbose;
	char	**e;

	if (!args || !environ)
		return ;
	++args;
	if (!(args = parse_flags(args, &ignore_env, &verbose)))
		return ;
	if (ignore_env)
	{
		if (verbose)
			ft_putendl("#env clearing environ");
		e = NULL;
	}
	else
		e = ft_arrdup_str(*environ);
	if (!(args = parse_vars(args, &e, verbose)))
		return ;
	if (*args)
		env_exec(args, &e, verbose);
	else
		list_env(e);
	ft_arrfree((void **)e);
}
