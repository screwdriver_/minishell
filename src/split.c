/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   split.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/24 16:57:57 by llenotre          #+#    #+#             */
/*   Updated: 2019/01/30 20:57:45 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static size_t	count_words(const char *str, t_bool (*f)(const char c))
{
	size_t words;

	words = 0;
	while (*str)
	{
		while (*str && f(*str))
			++str;
		if (!(*str))
			break ;
		++words;
		while (*str && !f(*str))
			++str;
	}
	return (words);
}

static size_t	wordlen(const char *str, t_bool (*f)(const char c))
{
	size_t len;

	len = 0;
	while (*str && f(*str))
		++str;
	while (*str && !f(*str))
	{
		++len;
		++str;
	}
	return (len);
}

char			**split(const char *str, t_bool (*f)(const char c))
{
	char	**buffer;
	size_t	words;
	size_t	i;
	size_t	len;

	words = count_words(str, f);
	if (!(buffer = ft_memalloc(sizeof(char *) * (words + 1))))
		return (NULL);
	i = 0;
	while (i < words)
	{
		while (*str && f(*str))
			++str;
		len = wordlen(str, f);
		if (!(buffer[i] = ft_strnew(len)))
		{
			ft_arrfree((void **)buffer);
			return (NULL);
		}
		ft_memcpy((void *)buffer[i], str, len);
		str += len;
		++i;
	}
	return (buffer);
}
