/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/24 17:36:32 by llenotre          #+#    #+#             */
/*   Updated: 2019/02/04 20:40:29 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void			exec_program(char **argv, char ***environ)
{
	pid_t	pid;
	int		err;

	if (!argv || !(*argv))
		return ;
	print_file_error(*argv, (err = check_file_error(*argv, TRUE, FALSE)), TRUE);
	if (err)
		return ;
	if ((pid = fork()) > 0)
	{
		g_current_proc = pid;
		waitpid(pid, NULL, 0);
		g_current_proc = 0;
		return ;
	}
	else if (pid < 0)
		return ;
	execve(*argv, (char *const *)argv, (char *const *)*environ);
	exit(0);
}

static const char	*get_bin_path(char **environ, const char *name, int *err)
{
	const char	*path;
	char		**paths;
	char		**p;

	*err = NOT_FOUND;
	path = NULL;
	if (!(paths = get_paths(environ)))
		return (NULL);
	p = paths;
	while (*p && *err == NOT_FOUND)
	{
		ft_memdel((void **)&path);
		path = ft_path_merge(*(p++), name);
		*err = check_file_error(path, TRUE, FALSE);
	}
	ft_arrfree((void **)paths);
	if (*err)
		ft_memdel((void **)&path);
	return (path);
}

static void			exec_bin(char **argv, char **environ)
{
	pid_t		pid;
	const char	*path;
	int			err;

	if (!argv || !(*argv))
		return ;
	if ((pid = fork()) > 0)
	{
		g_current_proc = pid;
		waitpid(pid, NULL, 0);
		g_current_proc = 0;
		return ;
	}
	else if (pid < 0)
		return ;
	if ((path = get_bin_path(environ, *argv, &err)))
		execve(path, (char *const *)argv, (char *const *)environ);
	else
		print_file_error(*argv, err, TRUE);
	ft_memdel((void **)&path);
	exit(0);
}

void				exec(char **argv, char ***environ)
{
	if (!argv || !(*argv))
		return ;
	if (ft_strequ(*argv, "echo"))
		c_echo(argv, environ);
	else if (ft_strequ(*argv, "cd"))
		c_cd(argv, environ);
	else if (ft_strequ(*argv, "setenv"))
		c_setenv(argv, environ);
	else if (ft_strequ(*argv, "unsetenv"))
		c_unsetenv(argv, environ);
	else if (ft_strequ(*argv, "env"))
		c_env(argv, environ);
	else if (ft_strequ(*argv, "exit"))
		exit(0);
	else if (**argv == '.' || **argv == '/')
		exec_program(argv, environ);
	else
		exec_bin((char **)argv, *environ);
}
