/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/23 17:20:07 by llenotre          #+#    #+#             */
/*   Updated: 2019/02/04 18:30:29 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# include "../libft/libft.h"
# include "../libft/get_next_line.h"

# include <fcntl.h>
# include <signal.h>
# include <stdint.h>
# ifdef __linux__
#  include <linux/limits.h>
# else
#  include <sys/syslimits.h>
# endif
# include <sys/stat.h>
# include <sys/wait.h>

# define OK					0
# define NOT_FOUND			1
# define PERMISSION_DENIED	2
# define NOT_DIRECTORY		3

pid_t	g_current_proc;

void	register_sig_handlers(void);

void	print_prompt(void);

void	parse(char ***environ, const char *command);

void	replace_escapes(char *str);

char	**split(const char *str, t_bool (*f)(const char c));

int		check_file_error(const char *path, const t_bool exec, const t_bool dir);
void	print_file_error(const char *file, const int err, const t_bool command);

void	exec(char **argv, char ***environ);

void	c_echo(char **args, char ***environ);
void	c_cd(char **args, char ***environ);
void	c_setenv(char **args, char ***environ);
void	c_unsetenv(char **args, char ***environ);
void	c_env(char **args, char ***environ);

char	**find_env_key(char **environ, const char *key);
char	*get_env_value(char **environ, const char *key);
void	set_env_value(char ***environ, const char *key, const char *value);
char	**get_paths(char **environ);

#endif
