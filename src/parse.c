/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/30 20:12:14 by llenotre          #+#    #+#             */
/*   Updated: 2019/01/31 17:40:28 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void		replace_vars(char **environ, char **str)
{
	char *s;
	char *s1;

	if (!str || !(*str))
		return ;
	if (!(s = ft_strchr(*str, '$')))
		return ;
	*(s++) = '\0';
	if (!(s1 = ft_strjoin(*str, get_env_value(environ, s))))
		return ;
	free((void *)*str);
	*str = s1;
}

static t_bool	is_space(const char c)
{
	return (c == ' ' || c == '\t');
}

void			parse(char ***environ, const char *command)
{
	char	**argv;
	char	**a;

	if (!(argv = split(command, is_space)))
		return ;
	a = argv;
	while (*a)
		replace_vars(*environ, a++);
	a = argv;
	while (*a)
		replace_escapes(*(a++));
	exec(argv, environ);
	ft_arrfree((void **)argv);
}
