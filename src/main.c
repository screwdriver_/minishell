/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/23 17:19:59 by llenotre          #+#    #+#             */
/*   Updated: 2019/02/04 20:08:58 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void		set_env_vars(char **argv, char ***environ)
{
	char buff[PATH_MAX];

	set_env_value(environ, "SHELL", *argv);
	set_env_value(environ, "PWD", getcwd(buff, sizeof(buff)));
}

void			print_prompt(void)
{
	ft_putstr("$> ");
}

static void		prompt(char ***environ)
{
	char	*command;

	if (get_next_line(0, &command) <= 0)
		exit(-1);
	parse(environ, command);
	free((void *)command);
}

int				main(int argc, char **argv, char **environ)
{
	(void)argc;
	environ = ft_arrdup_str(environ);
	g_current_proc = 0;
	set_env_vars(argv, &environ);
	register_sig_handlers();
	while (TRUE)
	{
		print_prompt();
		prompt(&environ);
	}
}
