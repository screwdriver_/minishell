/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/29 17:30:13 by llenotre          #+#    #+#             */
/*   Updated: 2019/02/04 20:39:50 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		check_file_error(const char *path, const t_bool exec, const t_bool dir)
{
	struct stat	s;

	if (!path)
		return (0);
	if (access(path, F_OK) < 0)
		return (NOT_FOUND);
	if (stat(path, &s) < 0)
		return (0);
	if (dir && ((s.st_mode & S_IFMT) != S_IFDIR))
		return (access(path, R_OK) == 0 ? NOT_DIRECTORY : PERMISSION_DENIED);
	if (exec && ((s.st_mode & S_IFMT) == S_IFDIR
		|| access(path, R_OK | X_OK) < 0))
		return (PERMISSION_DENIED);
	return (access(path, R_OK) == 0 ? OK : PERMISSION_DENIED);
}

void	print_file_error(const char *file, const int err, const t_bool command)
{
	if (!file || !err)
		return ;
	if (err != NOT_DIRECTORY)
	{
		ft_putstr_fd(file, 2);
		ft_putstr_fd(": ", 2);
	}
	if (err == NOT_FOUND)
		ft_putendl_fd(command ? "command not found"
			: "No such file or directory", 2);
	else if (err == PERMISSION_DENIED)
		ft_putendl_fd("permission denied", 2);
	else if (err == NOT_DIRECTORY)
	{
		ft_putstr_fd("not a directory: ", 2);
		ft_putendl_fd(file, 2);
	}
	else
		ft_putendl_fd("unknown error", 2);
}
