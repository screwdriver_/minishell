/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_var.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/24 15:04:06 by llenotre          #+#    #+#             */
/*   Updated: 2019/02/04 18:11:46 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char			**find_env_key(char **environ, const char *key)
{
	if (!environ || !key)
		return (NULL);
	while (*environ)
	{
		if (ft_strcmp(*environ, key) == '=')
			return (environ);
		++environ;
	}
	return (NULL);
}

char			*get_env_value(char **environ, const char *key)
{
	char	**s;

	if (!(s = find_env_key(environ, key)))
		return (NULL);
	return (*s + ft_strlen(key) + 1);
}

void			set_env_value(char ***environ,
	const char *key, const char *value)
{
	char	*s;
	char	**k;
	char	**a;

	if (!environ || !key || !value)
		return ;
	if (!(s = ft_strnjoin(3, key, "=", value)))
		return ;
	if ((k = find_env_key(*environ, key)))
	{
		free((void*)*k);
		*k = s;
		return ;
	}
	if (!(a = (char **)ft_arrinsert((void **)*environ, s)))
		return ;
	*environ = a;
}

static t_bool	is_path_separator(const char c)
{
	return (c == ':');
}

char			**get_paths(char **environ)
{
	char	*path;

	if (!(path = get_env_value(environ, "PATH")))
		return (NULL);
	return (split(path, is_path_separator));
}
