/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   signal.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/29 15:41:51 by llenotre          #+#    #+#             */
/*   Updated: 2019/02/04 19:20:20 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	sigint_handler(int sig)
{
	(void)sig;
	if (!g_current_proc)
	{
		ft_putchar('\n');
		print_prompt();
	}
}

void	register_sig_handlers(void)
{
	signal(SIGINT, sigint_handler);
	signal(SIGCHLD, sigint_handler);
}
