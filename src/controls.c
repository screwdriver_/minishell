/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   controls.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: llenotre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/29 14:22:11 by llenotre          #+#    #+#             */
/*   Updated: 2019/01/29 15:39:11 by llenotre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static char	replace_char(const char from)
{
	if (from == 'n')
		return ('\n');
	else if (from == 'r')
		return ('\r');
	else if (from == 't')
		return ('\t');
	else if (from == 'b')
		return ('\b');
	else if (from == 'f')
		return ('\f');
	else if (from == 'v')
		return ('\v');
	else if (from == '0')
		return ('\0');
	else
		return (from);
}

void		replace_escapes(char *str)
{
	while (*str && str[1])
	{
		if (*str == '\\')
		{
			*str = replace_char(str[1]);
			ft_memmove(str + 1, str + 2, ft_strlen(str));
		}
		++str;
	}
}
